const canvas = document.getElementById('pong');
const ctx = canvas.getContext('2d');

// Set text alignment settings.
ctx.textBaseline = 'middle';
ctx.textAlign = 'center';
const defaultSize = 640;

const globalColor = 'lime';

let userScore = 0;
let comScore = 0;
let comSpeed = 0.2;

// Pause game on startup.
let paused = true;
let pauseText = 'CLICK TO PLAY';

function randomInt(min, max, exclude) {
  min = Math.ceil(min);
  max = Math.floor(max);
  ans = Math.floor(Math.random() * (max - min + 1) + min);
  if (ans == exclude) return randomInt(min, max, exclude);
  else return ans;
}

// Randomly set ball velocity on startup.
const ball = {
  x: canvas.width / 2,
  y: canvas.height / 2,
  radius: 10,
  velocityX: randomInt(-1, 1, 0) * 10 - 1,
  velocityY: randomInt(-1, 1, 0) * 10 + 2,
  speed: 10,
};
// Dotted lines down middle of playing field.
const net = {
  x: (canvas.width - 5) / 2,
  y: 0,
  height: 10,
  width: 5,
};

// Add left and top user.
const userL = {
  // Both users 15px infront of border.
  x: 15,
  y: (canvas.height - 100) / 2,
  width: 10,
  height: 100,
};
const userT = {
  x: (canvas.width - 100) / 2,
  y: 15,
  width: 100,
  height: 10,
};

// Add right and bottom computer.
const comR = {
  // Float computer 15px behind border.
  x: canvas.width - 25,
  y: (canvas.height - 100) / 2,
  width: 10,
  height: 100,
};
const comB = {
  x: (canvas.width - 100) / 2,
  // Float computer 15px above border.
  y: canvas.height - 25,
  width: 100,
  height: 10,
};

// Handle mouse movement for whole window to prevent cut-off.
window.addEventListener('mousemove', getMousePos);
// Handle key presses for moving paddles with keyboard.
window.addEventListener('keydown', handleKeys);
// Handle pausing by toggling canvas update() function on mouse click.
canvas.addEventListener('click', () => {
  pauseText = 'CLICK TO PLAY';
  paused = !paused;
});

function getMousePos(evt) {
  // Get new canvas rectangle each time mouse moves to accommodate for window size changes.
  const rect = canvas.getBoundingClientRect();
  // Move both users here.
  userL.y = evt.clientY - rect.top - userL.height / 2;
  userT.x = evt.clientX - rect.left - userT.width / 2;
}

// Control paddles with arrow keys or WASD.
function handleKeys(press) {
  switch (press.key) {
    case 'ArrowUp':
    case 'Up':
    case 'w':
      userL.y -= canvas.height / 20;
      break;
    case 'ArrowDown':
    case 'Down':
    case 's':
      userL.y += canvas.height / 20;
      break;
    case 'ArrowLeft':
    case 'Left':
    case 'a':
      userT.x -= canvas.width / 20;
      break;
    case 'ArrowRight':
    case 'Right':
    case 'd':
      userT.x += canvas.width / 20;
      break;
    // Computer speed changing from 0.0 [disabled] to 0.9.
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
    case '0':
      comSpeed = (press.keyCode - 48) / 10;
      // Reset score after difficulty change.
      userScore = 0;
      comScore = 0;
      break;
  }
}

// Various sound libraries.
function playHit() {
  switch (randomInt(0, 2)) {
    case 0:
      new Audio('sounds/hit1.wav').play();
      break;
    case 1:
      new Audio('sounds/hit2.wav').play();
      break;
    case 2:
      new Audio('sounds/hit3.wav').play();
      break;
  }
}
function playCom() {
  if (randomInt(0, 1)) new Audio('sounds/comscore1.wav').play();
  else new Audio('sounds/comscore2.wav').play();
}
function playUser() {
  if (randomInt(0, 1)) new Audio('sounds/playerscore1.wav').play();
  else new Audio('sounds/playerscore2.wav').play();
}

// Clear previous screen elements through filling transparent rectangle.
function clearScreen() {
  // Randomly decide fill strength between 0.25 and 0.5.
  ctx.globalAlpha = randomInt(25, 50) / 100;
  ctx.fillStyle = 'black';
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  ctx.globalAlpha = 1.0;
}

function drawRect(x, y, w, h, color) {
  ctx.fillStyle = color;
  ctx.fillRect(x, y, w, h);
}

function drawArc(x, y, r, color) {
  ctx.fillStyle = color;
  ctx.beginPath();
  ctx.arc(x, y, r, 0, Math.PI * 2, true);
  ctx.closePath();
  ctx.fill();
}

function drawNet() {
  for (let i = 0; i <= canvas.height; i += 15)
    drawRect(net.x, net.y + i, net.width, net.height, globalColor);
}

// Draw text with font size that scales if the canvas size is changed.
function drawText(text, x, y, color, fontScale) {
  ctx.fillStyle = color;
  ctx.font = `${(fontScale / defaultSize) * canvas.width}px courier`;
  ctx.fillText(text, x, y);
}

// Determine if ball has contacted a paddle.
function collision(b, p) {
  p.top = p.y;
  p.bottom = p.y + p.height;
  p.left = p.x;
  p.right = p.x + p.width;

  b.top = b.y - b.radius;
  b.bottom = b.y + b.radius;
  b.left = b.x - b.radius;
  b.right = b.x + b.radius;

  return (
    p.left < b.right && p.top < b.bottom && p.right > b.left && p.bottom > b.top
  );
}

// Reset ball to starting position, and pause game.
function resetBall() {
  ball.x = canvas.width / 2;
  ball.y = canvas.height / 2;
  // Send ball in opposite direction.
  ball.velocityX = -ball.velocityX;
  ball.velocityY = -ball.velocityY;
  ball.speed = 10;
  paused = true;
}

function comWin() {
  comScore++;
  pauseText = 'COM SCORED';
  playCom();
  resetBall();
}

function userWin() {
  userScore++;
  pauseText = 'USER SCORED';
  playUser();
  resetBall();
}

function update() {
  // Globally prevent player from leaving play space.
  // Bottom-left.
  if (userL.y + userL.height > canvas.height)
    userL.y = canvas.height - userL.height;
  // Top-left.
  if (userL.y < 0) userL.y = 0;
  // Top-right.
  if (userT.x + userT.width > canvas.width)
    userT.x = canvas.width - userT.width;
  // Top-left (alternate user).
  if (userT.x < 0) userT.x = 0;

  // Display goals visually, and increment scores.
  if (ball.x - ball.radius < 0) {
    // Left
    comWin();
    // Draw walls when goal is made.
    drawRect(0, 0, canvas.width / 25, canvas.height, 'yellow');
  } else if (ball.y - ball.radius < 0) {
    // Top
    comWin();
    drawRect(0, 0, canvas.width, canvas.height / 25, 'yellow');
  } else if (ball.x + ball.radius > canvas.width) {
    // Right
    userWin();
    drawRect(
      canvas.width - canvas.width / 25,
      0,
      canvas.width / 25,
      canvas.height,
      'red'
    );
  } else if (ball.y + ball.radius > canvas.height) {
    // Bottom
    userWin();
    drawRect(
      0,
      canvas.height - canvas.height / 25,
      canvas.width,
      canvas.height / 25,
      'red'
    );
  }

  // Manage ball movement.
  ball.x += ball.velocityX;
  ball.y += ball.velocityY;

  // Manage computer AI.
  comR.y += (ball.y - (comR.y + comR.height / 2)) * comSpeed;
  comB.x += (ball.x - (comB.x + comB.width / 2)) * comSpeed;

  let paddleLR = ball.x + ball.radius < canvas.width / 2 ? userL : comR;
  let paddleTB = ball.y + ball.radius < canvas.height / 2 ? userT : comB;

  // Manage collision for left user and right computer.
  if (collision(ball, paddleLR)) {
    // Play sound effect.
    playHit();

    let collidePoint = ball.y - (paddleLR.y + paddleLR.height / 2);
    collidePoint = collidePoint / (paddleLR.height / 2);

    let angleRad = (Math.PI / 4) * collidePoint;

    let direction = ball.x + ball.radius < canvas.width / 2 ? 1 : -1;
    ball.velocityX = direction * ball.speed * Math.cos(angleRad);
    ball.velocityY = ball.speed * Math.sin(angleRad);

    // Increment ball speed by a quarter.
    ball.speed += 0.25;
  }

  // Manage collision for top user and bottom computer.
  if (collision(ball, paddleTB)) {
    playHit();

    let collidePoint = ball.x - (paddleTB.x + paddleTB.width / 2);
    collidePoint = collidePoint / (paddleTB.width / 2);

    let angleRad = (Math.PI / 4) * collidePoint;

    let direction = ball.y + ball.radius < canvas.height / 2 ? 1 : -1;
    // Flip x and y velocity values for top and bottom paddles.
    ball.velocityX = ball.speed * Math.sin(angleRad);
    ball.velocityY = direction * ball.speed * Math.cos(angleRad);

    ball.speed += 0.25;
  }
}

function render() {
  // Partially clear screen every frame.
  if (!paused) clearScreen();
  // Draw text asking to unpause.
  else
    return drawText(
      pauseText,
      canvas.width / 2,
      canvas.height / 2,
      'white',
      75
    );

  // Draw title at top and bottom in dark green.
  drawText('Pokonkt', canvas.width / 2, canvas.height / 3, 'darkgreen', 150);
  drawText(
    'Pokonkt',
    canvas.width / 2,
    (canvas.height / 3) * 2,
    'darkgreen',
    150
  );

  // Center scores for user in top-left corner.
  drawText(
    'U-' + userScore,
    canvas.width / 8,
    canvas.height / 8,
    globalColor,
    60
  );
  // Center scores for computer in bottom-right corner.
  drawText(
    'C-' + comScore,
    (canvas.width / 8) * 7,
    (canvas.height / 8) * 7,
    globalColor,
    60
  );

  // Draw computer speed level.
  drawText(
    'COM SPEED: ' + comSpeed,
    (canvas.width / 10) * 8.5,
    (canvas.height / 10) * 9.5,
    globalColor,
    15
  );
  // Dotted lines down middle.
  drawNet();

  // Draw both users.
  drawRect(userL.x, userL.y, userL.width, userL.height, globalColor);
  drawRect(userT.x, userT.y, userT.width, userT.height, globalColor);

  // Draw both computers.
  drawRect(comR.x, comR.y, comR.width, comR.height, globalColor);
  drawRect(comB.x, comB.y, comB.width, comB.height, globalColor);

  drawArc(ball.x, ball.y, ball.radius, globalColor);
}

function game() {
  // Update game state only if not paused.
  if (!paused) update();

  render();
}

// Run at 24FPS.
let loop = setInterval(game, 1000 / 24);
